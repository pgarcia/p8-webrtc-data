import asyncio

from aiortc import RTCIceCandidate, RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.signaling import BYE


async def consume_signaling(pc, signaling):
    while True:
        obj = await signaling.receive()

        if isinstance(obj, RTCSessionDescription):
            await pc.setRemoteDescription(obj)

            if obj.type == "offer":
                # send answer
                await pc.setLocalDescription(await pc.createAnswer())
                await signaling.send(pc.localDescription)
        elif obj is BYE:
            print("Exiting")
            break


time_start = None


async def run_answer(pc, signaling):
    await signaling.connect()

    @pc.on("datachannel")
    def on_datachannel(channel):
        print(f"channel({channel.label}) > created by remote party")

        @channel.on("message")
        def on_message(message):
            print(f"channel({channel.label}) > {message}")

            if isinstance(message, str) and message.startswith("ping"):
                # reply
                message = f"pong{message[4:]}"
                print(f"channel({channel.label}) > {message}")
                channel.send(message)

    # Register the server
    signaling.send("REGISTER SERVER".encode("utf-8"))

    await consume_signaling(pc, signaling)


if __name__ == "__main__":
    pc = RTCPeerConnection()
    asyncio.run(run_answer(pc, signaling))
