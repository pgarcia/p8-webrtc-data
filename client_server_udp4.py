import asyncio
import json
import time
from aiortc import RTCIceCandidate, RTCPeerConnection, RTCSessionDescription


async def start_signaling(message):
    while UDPSignaling.noACK:
        print('Send:', message)
        UDPSignaling.transport.sendto(message.encode())
        await asyncio.sleep(2)

async def consume_signaling(data):
    global pc, new_peer
    try:
        jsonMessage = json.loads(data)
    except json.JSONDecodeError:
        if data == "ACK":
            UDPSignaling.noACK = False
            print("ACK received")
            return

    if jsonMessage["type"] == "bye":
        print("Connection Finnished")
        return
    else:
        obj = RTCSessionDescription(**jsonMessage)
        if isinstance(obj, RTCSessionDescription):
            new_peer = True
            await asyncio.sleep(0.3)
            await open_connection(pc)
            await pc[peer_numb].setRemoteDescription(obj)

            if obj.type == "offer":
                await pc[peer_numb].setLocalDescription(await pc[peer_numb].createAnswer())
                UDPSignaling.transport.sendto(json.dumps
                                              ({"sdp": pc[peer_numb].localDescription.sdp,
                                                "type": pc[peer_numb].localDescription.type}).encode())

async def open_connection(pc):

    @pc[peer_numb].on("datachannel")
    def on_datachannel(channel):
        print(f"channel({channel.label}) > created by remote party")

        @channel.on("message")
        def on_message(message):

            print(f"channel({channel.label}) > {message}")

            if isinstance(message, str) and message.startswith("ping"):
                # reply
                message = f"pong{message[4:]}"
                print(f"channel({channel.label}) > {message}")
                channel.send(message)

time_start = None


class UDPSignaling:
    noACK = True
    transport = None
    address = ("127.0.0.1", 9999)
    def __init__(self, message, on_con_lost, pc):
        self.pc = pc
        self.message = message
        self.on_con_lost = on_con_lost
        self.transport = None

    def connection_made(self, transport):
        UDPSignaling.transport = transport
        asyncio.ensure_future(start_signaling(self.message))


    def datagram_received(self, data, addr):
        print("Received:", data.decode())
        asyncio.ensure_future(consume_signaling(data.decode()))


    def error_received(self, exc):
        print('Error received:', exc)

    def connection_lost(self, exc):
        print("Connection closed")
        self.on_con_lost.set_result(True)


async def connect(pc):

    loop = asyncio.get_running_loop()

    on_con_lost = loop.create_future()
    message = "REGISTER SERVER"                                               
    transport, protocol = await loop.create_datagram_endpoint(                  
        lambda: UDPSignaling(message, on_con_lost, pc),
        remote_addr=UDPSignaling.address)

    try:
        await on_con_lost
    finally:
        transport.close()


peer_numb = -1
new_peer = False
async def tooManyPeers():
    global peer_numb, new_peer, pc
    while True:
        if new_peer:
            pc.append(RTCPeerConnection())
            peer_numb += 1
            new_peer = False
        await asyncio.sleep(0.1)




async def run_answer(pc, signaling):
    await signaling

if __name__ == "__main__":
    pc = []
    asyncio.ensure_future(tooManyPeers())
    signaling = connect(pc)
    coro = run_answer(pc, signaling)

    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(coro)
    except KeyboardInterrupt:
        pass
    finally:
        for i in range(len(pc)):
            loop.run_until_complete(pc[i].close())

