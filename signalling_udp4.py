import asyncio
import json


async def reception(data, addr, transport):
    message = data.decode()
    print('Received %r from %s' % (message, addr))

    if message == 'REGISTER CLIENT':
        SignallingProtocol.members["clientAddress"] = addr
        print(f"Client registered at {addr}")
        transport.sendto("ACK".encode(), SignallingProtocol.members["clientAddress"])
        if SignallingProtocol.the_server_is_up:
            transport.sendto("GO AHEAD".encode(), SignallingProtocol.members["clientAddress"])



    elif message == 'REGISTER SERVER':
        SignallingProtocol.members["serverAddress"] = addr
        print(f"Server registered at {addr}")
        transport.sendto("ACK".encode(), SignallingProtocol.members["serverAddress"])
        if SignallingProtocol.members["clientAddress"] is None:
            while SignallingProtocol.members["clientAddress"] is None:
                await asyncio.sleep(0.1)
        transport.sendto("GO AHEAD".encode(), SignallingProtocol.members["clientAddress"])
        SignallingProtocol.the_server_is_up = True


    else:

        try:
            jsonMessage = json.loads(message)
            print("JSON received")
        except ValueError:
            print("Invalid JSON")
            return
        kind = jsonMessage['type']
        if kind == 'answer':
            print('Send %r to %s' % (kind, SignallingProtocol.members["clientAddress"]))
            transport.sendto(data, SignallingProtocol.members["clientAddress"])

        elif kind == 'offer':
            print('Send %r to %s' % (kind, SignallingProtocol.members["serverAddress"]))
            transport.sendto(data, SignallingProtocol.members["serverAddress"])
        elif kind == 'bye':
            print('Send %r to %s' % (kind, SignallingProtocol.members["serverAddress"]))
            transport.sendto(data, SignallingProtocol.members["serverAddress"])
            SignallingProtocol.members["clientAddress"] = None



class SignallingProtocol:
    members = {"clientAddress": None, "serverAddress": None}
    the_server_is_up = False
    def __init__(self):
        self.transport = None
        self.members = {"clientAddress": None, "serverAddress": None}

    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        asyncio.ensure_future(reception(data, addr, self.transport))







async def main():
    print("Starting UDP server")

    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    loop = asyncio.get_running_loop()

    # One protocol instance will be created to serve all
    # client requests.
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: SignallingProtocol(),
        local_addr=('127.0.0.1', 9999))

    try:
        await asyncio.sleep(3600)  # Serve for 1 hour.
    finally:
        transport.close()


asyncio.run(main())
